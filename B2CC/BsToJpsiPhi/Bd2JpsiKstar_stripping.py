import os
import sys
sys.path.append(os.path.join(os.environ['ANALYSIS_PRODUCTIONS_BASE'], 'B2CC/BsToJpsiPhi'))

from Configurables import DaVinci

from helpers.stripping import stripping
from generate_configuration import bookkeeping_data

stripping_line = 'BetaSBd2JpsiKstarDetachedLine'
data_type = DaVinci().DataType
stripping_version = bookkeeping_data[data_type].strip

seq = stripping(stripping_version, stripping_line)
DaVinci().UserAlgorithms = [seq]
