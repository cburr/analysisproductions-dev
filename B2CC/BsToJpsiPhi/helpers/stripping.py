from Configurables import (
    DaVinci,
    GaudiSequencer,
    EventNodeKiller,
    ProcStatusCheck
)
from StrippingConf.Configuration import (
    StrippingConf,
    StrippingStream
)
from StrippingSettings.Utils import strippingConfiguration
from StrippingArchive.Utils import buildStreams
from StrippingArchive import strippingArchive

from DSTWriters.microdstelements import *
from DSTWriters.Configuration import (
    SelDSTWriter,
    stripDSTStreamConf,
    stripDSTElements
)


def stripping(stripping_version, stripping_line):
    # kill the banks
    event_node_killer = EventNodeKiller('StripKiller')
    event_node_killer.Nodes = ['/Event/AllStreams', '/Event/Strip']

    # build streams
    streams = buildStreams(stripping=strippingConfiguration(stripping_version),
                           archive=strippingArchive(stripping_version))
    # declare your custom stream
    custom_stream = StrippingStream('AllStreams')
    custom_line = 'Stripping'+stripping_line

    for stream in streams:
        for sline in stream.lines:
            if sline.name() == custom_line:
                sline._prescale = 1.0
                custom_stream.appendLines([sline])

    filterBadEvents = ProcStatusCheck()

    # configure your custom stream
    sc = StrippingConf(Streams=[custom_stream],
                       MaxCandidates=2000,
                       AcceptBadEvents=False,
                       BadEventSelection=filterBadEvents)

    enablePacking = False

    SelDSTWriterElements = {'default': stripDSTElements(pack=enablePacking)}

    SelDSTWriterConf = {'default': stripDSTStreamConf(pack=enablePacking,
                                                      selectiveRawEvent=True,
                                                      fileExtension='.anaprod_bstojpsiphi.ldst')}

    dstWriter = SelDSTWriter('MyDSTWriter',
                             StreamConf=SelDSTWriterConf,
                             MicroDSTElements=SelDSTWriterElements,
                             OutputFileSuffix='',
                             SelectionSequences=sc.activeStreams()
                             )

    DaVinci().ProductionType = 'Stripping'
    seq = GaudiSequencer('TupleSeq')
    seq.IgnoreFilterPassed = True
    seq.Members += [event_node_killer, sc.sequence()] + [dstWriter.sequence()]

    return seq
