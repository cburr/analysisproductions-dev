#!/usr/bin/env python
import json
from collections import namedtuple

periods = ['2015', '2016', '2017', '2018'] # ['2011', '2012', '2015', '2016', '2017', '2018']
output_json = 'info.json'
dq_flag = 'OK'
application = 'Castelao'
application_version = 'v2r1'
output_type = 'B2CC_BSTOJPSIPHI_DVNTUPLE.ROOT'

streams = ['DIMUON', 'LEPTONIC', 'BHADRONCOMPLETEEVENT']
dimuon = ['Bs2JpsiPhi.py', 'Bd2JpsiKstar.py', 'Bu2JpsiKplus.py']
leptonic = ['Bs2JpsiPhi_Prompt.py', 'Bs2JpsiPhi_Prompt_PVMixer.py']
bhadron = ['Bs2DsPi.py']

bkk_data = namedtuple('Data', 'beam reco strip davinci')
bookkeeping_data = {
    '2011': bkk_data('3500', 'Reco14', 'Stripping21r1', ''),
    '2012': bkk_data('4000', 'Reco14', 'Stripping21', ''),
    '2015': bkk_data('6500', 'Reco15a', 'Stripping24r1', ''),
    '2016': bkk_data('6500', 'Reco16', 'Stripping28r1', ''),
    '2017': bkk_data('6500', 'Reco17', 'Stripping29r2', 'v42r7p2'),
    '2018': bkk_data('6500', 'Reco18', 'Stripping34', 'v44r4')
}

bkk_mc = namedtuple('MC', 'mc_type sim trigreco strip eventtype dst option_file dddb conddb pythia')
bookkeeping_mc = {

    '2015': [bkk_mc('JpsiPhi', 'Sim09b', 'Trig0x411400a2/Reco15a/Turbo02', 'Stripping24', '13144011', 'DST', 'Bs2JpsiPhi.py', 'dddb-20150724', 'sim-20161124', 'Pythia8'),
             bkk_mc('JpsiPhi_dg0', 'Sim09b', 'Trig0x411400a2/Reco15a/Turbo02', 'Stripping24', '13144004', 'MDST', 'Bs2JpsiPhi.py', 'dddb-20150724', 'sim-20161124', 'Pythia8'),
             bkk_mc('JpsiKstar_ldst', 'Sim09b', 'Trig0x411400a2/Reco15a/Turbo02', 'Stripping24', '11144001', 'LDST', 'Bd2JpsiKstar.py', 'dddb-20150724', 'sim-20161124', 'Pythia8'),
             bkk_mc('JpsiKstar_mdst', 'Sim09c', 'Trig0x411400a2/Reco15a/Turbo02', 'Stripping24r1', '11144001', 'MDST', 'Bd2JpsiKstar.py', 'dddb-20170721-3', 'sim-20161124', 'Pythia8'),
             bkk_mc('JpsiKstar_dst', 'Sim09c', 'Trig0x411400a2/Reco15a/Turbo02', 'Stripping24r1', '11144001', 'DST', 'Bd2JpsiKstar.py', 'dddb-20170721-3', 'sim-20161124', 'Pythia8'),
             bkk_mc('JpsiKplus_ldst', 'Sim09b', 'Trig0x411400a2/Reco15a/Turbo02', 'Stripping24', '12143001', 'LDST', 'Bu2JpsiKplus.py', 'dddb-20150724', 'sim-20161124', 'Pythia8'),
             bkk_mc('JpsiKplus_sim09b', 'Sim09b', 'Trig0x411400a2/Reco15a/Turbo02', 'Stripping24', '12143001', 'DST', 'Bu2JpsiKplus.py', 'dddb-20150724', 'sim-20161124', 'Pythia8'),
             bkk_mc('JpsiKplus_sim09c', 'Sim09c', 'Trig0x411400a2/Reco15a/Turbo02', 'Stripping24r1', '12143001', 'DST', 'Bu2JpsiKplus.py', 'dddb-20170721-3', 'sim-20161124', 'Pythia8'),
             bkk_mc('InclJpsi', 'Sim09b', 'Trig0x411400a2/Reco15a/Turbo02', 'Stripping24', '24142001', 'DST', 'Bs2JpsiPhi_Prompt.py', 'dddb-20150724', 'sim-20161124', 'Pythia8'),
             bkk_mc('LbJpsipK', 'Sim09b', 'Trig0x411400a2/Reco15a/Turbo02', 'Stripping24', '15144001', 'DST', 'Bs2JpsiPhi.py', 'dddb-20150724', 'sim-20161124', 'Pythia8'),
             ],

    '2016': [bkk_mc('JpsiPhi_str26', 'Sim09b', 'Trig0x6138160F/Reco16/Turbo03', 'Stripping26', '13144011', 'MDST', 'Bs2JpsiPhi.py', 'dddb-20150724', 'sim-20161124-2', 'Pythia8'),
             bkk_mc('JpsiPhi_str28_sim09b', 'Sim09b', 'Trig0x6138160F/Reco16/Turbo03', 'Stripping28', '13144011', 'DST', 'Bs2JpsiPhi.py', 'dddb-20150724', 'sim-20161124-2', 'Pythia8'),
             bkk_mc('JpsiPhi_str28_sim09c', 'Sim09c', 'Trig0x6138160F/Reco16/Turbo03', 'Stripping28', '13144011', 'DST', 'Bs2JpsiPhi.py', 'dddb-20170721-3', 'sim-20170721-2', 'Pythia8'),
             bkk_mc('JpsiPhi_dg0', 'Sim09b', 'Trig0x6138160F/Reco16/Turbo03', 'Stripping26', '13144004', 'MDST', 'Bs2JpsiPhi.py', 'dddb-20150724', 'sim-20161124-2', 'Pythia8'),
             bkk_mc('JpsiKstar_dst', 'Sim09b', 'Trig0x6138160F/Reco16/Turbo03', 'Stripping26', '11144001', 'DST', 'Bd2JpsiKstar.py', 'dddb-20150724', 'sim-20161124-2', 'Pythia8'),
             bkk_mc('JpsiKstar_ldst', 'Sim09b', 'Trig0x6138160F/Reco16/Turbo03', 'Stripping26', '11144001', 'LDST', 'Bd2JpsiKstar.py', 'dddb-20150724', 'sim-20161124-2', 'Pythia8'),
             bkk_mc('JpsiKstar_mdst', 'Sim09c', 'Trig0x6138160F/Reco16/Turbo03', 'Stripping28r1', '11144001', 'MDST', 'Bd2JpsiKstar.py', 'dddb-20170721-3', 'sim-20170721-2', 'Pythia8'),
             bkk_mc('JpsiKplus', 'Sim09b', 'Trig0x6138160F/Reco16/Turbo03', 'Stripping26', '12143001', 'DST', 'Bu2JpsiKplus.py', 'dddb-20150724', 'sim-20161124-2', 'Pythia8'),
             bkk_mc('JpsiKplus_ldst', 'Sim09b', 'Trig0x6138160F/Reco16/Turbo03', 'Stripping26', '12143001', 'LDST', 'Bu2JpsiKplus.py', 'dddb-20150724', 'sim-20161124-2', 'Pythia8'),
             bkk_mc('JpsiKplus_sim09e', 'Sim09e', 'Trig0x6139160F/Reco16/Turbo03', 'Stripping28r1', '12143001', 'LDST', 'Bu2JpsiKplus.py', 'dddb-20170721-3', 'sim-20170721-2', 'Pythia8'),
             bkk_mc('InclJpsi', 'Sim09b', 'Trig0x6138160F/Reco16/Turbo03', 'Stripping26', '24142001', 'DST', 'Bs2JpsiPhi_Prompt.py', 'dddb-20150724', 'sim-20161124-2', 'Pythia8'),
             bkk_mc('LbJpsipK', 'Sim09b', 'Trig0x6138160F/Reco16/Turbo03', 'Stripping26', '15144001', 'DST', 'Bs2JpsiPhi.py', 'dddb-20150724', 'sim-20161124-2', 'Pythia8'),
             bkk_mc('DsPi_sim09b', 'Sim09b', 'Trig0x6138160F/Reco16/Turbo03', 'Stripping26', '13264021', 'DST', 'Bs2DsPi.py', 'dddb-20150724', 'sim-20161124-2', 'Pythia8'),
             bkk_mc('DsPi_sim09e', 'Sim09e', 'Trig0x6139160F/Reco16/Turbo03', 'Stripping28r1', '13264021', 'LDST', 'Bs2DsPi.py', 'dddb-20170721-3', 'sim-20170721-2', 'Pythia8'),
             ],

    '2017': [bkk_mc('JpsiPhi', 'Sim09f', 'Trig0x62661709/Reco17/Turbo04a-WithTurcal', 'Stripping29r2', '13144011', 'DST', 'Bs2JpsiPhi.py', 'dddb-20170721-3', 'sim-20180411', 'Pythia8'),
             bkk_mc('JpsiKstar', 'Sim09f', 'Trig0x62661709/Reco17/Turbo04a-WithTurcal', 'Stripping29r2', '11144001', 'DST', 'Bd2JpsiKstar.py', 'dddb-20170721-3', 'sim-20180411', 'Pythia8'),
             # bkk_mc('JpsiKstar_no_str_e', 'Sim09e', 'Trig0x62661709/Reco17', '', '11144001', 'LDST', 'Bd2JpsiKstar.py', 'dddb-20170721-3', 'sim-20180411', 'Pythia8'),
             bkk_mc('JpsiKstar_no_str_g', 'Sim09g', 'Trig0x62661709/Reco17', '', '11144001', 'LDST', 'Bd2JpsiKstar.py', 'dddb-20170721-3', 'sim-20190430-1', 'Pythia8'),
             bkk_mc('JpsiKplus_no_str_e', 'Sim09e', 'Trig0x62661709/Reco17', '', '12143001', 'LDST', 'Bu2JpsiKplus.py', 'dddb-20170721-3', 'sim-20180411', 'Pythia8'),
             # bkk_mc('JpsiKplus_no_str_g', 'Sim09g', 'Trig0x62661709/Reco17', '', '12143001', 'LDST', 'Bu2JpsiKplus.py', 'dddb-20170721-3', 'sim-20190430-1', 'Pythia8'),
             bkk_mc('JpsiPhi_largeLifetime', 'Sim09f', 'Trig0x62661709/Reco17/Turbo04a-WithTurcal', 'Stripping29r2', '13144016', 'DST', 'Bs2JpsiPhi.py', 'dddb-20170721-3', 'sim-20180411', 'Pythia8'),
             bkk_mc('LbJpsipK', 'Sim09e', 'Trig0x62661709/Reco17/Turbo04a-WithTurcal', 'Stripping29r2', '15144001', 'DST', 'Bs2JpsiPhi.py', 'dddb-20170721-3', 'sim-20180411', 'Pythia8'),
             bkk_mc('Bspi2JpsiPhi', 'Sim09f', 'Trig0x62661709/Reco17/Turbo04a-WithTurcal', 'Stripping29r2', '14135000', 'DST', 'Bc2Bspi2JpsiPhi.py', 'dddb-20170721-3', 'sim-20180411', 'BcVegPyPythia8'),
             ],

    '2018': [bkk_mc('JpsiPhi', 'Sim09f', 'Trig0x617d18a4/Reco18/Turbo05-WithTurcal', 'Stripping34', '13144011', 'DST', 'Bs2JpsiPhi.py', 'dddb-20170721-3', 'sim-20190128', 'Pythia8'),
             bkk_mc('JpsiKstar', 'Sim09f', 'Trig0x617d18a4/Reco18/Turbo05-WithTurcal', 'Stripping34', '11144001', 'DST', 'Bd2JpsiKstar.py', 'dddb-20170721-3', 'sim-20190128', 'Pythia8'),
             # bkk_mc('JpsiKstar_no_str_f', 'Sim09f', 'Trig0x617d18a4/Reco18', '', '11144001', 'LDST', 'Bd2JpsiKstar.py', 'dddb-20170721-3', 'sim-20190128', 'Pythia8'),
             # bkk_mc('JpsiKstar_no_str_g', 'Sim09g', 'Trig0x617d18a4/Reco18', '', '11144001', 'LDST', 'Bd2JpsiKstar.py', 'dddb-20170721-3', 'sim-20190430', 'Pythia8'),
             # bkk_mc('JpsiKplus_no_str_f', 'Sim09f', 'Trig0x617d18a4/Reco18', '', '12143001', 'LDST', 'Bu2JpsiKplus.py', 'dddb-20170721-3', 'sim-20190128', 'Pythia8'),
             # bkk_mc('JpsiKplus_no_str_g', 'Sim09g', 'Trig0x617d18a4/Reco18', '', '12143001', 'LDST', 'Bu2JpsiKplus.py', 'dddb-20170721-3', 'sim-20190430', 'Pythia8'),
             bkk_mc('LbJpsipK', 'Sim09f', 'Trig0x617d18a4/Reco18/Turbo05-WithTurcal', 'Stripping34', '15144001', 'DST', 'Bs2JpsiPhi.py', 'dddb-20170721-3', 'sim-20190128', 'Pythia8'),
             ],
}

if __name__ == '__main__':
    config_dict = dict()
    for year in periods:
        for magnet in ['MagUp', 'MagDown']:
            for stream in streams:
                config_dict['{}_{}_{}'.format(year, magnet, stream)] = dict()
                config_dict['{}_{}_{}'.format(year, magnet, stream)]['dq_flag'] = dq_flag
                config_dict['{}_{}_{}'.format(year, magnet, stream)]['application'] = application
                config_dict['{}_{}_{}'.format(year, magnet, stream)]['application_version'] = application_version
                config_dict['{}_{}_{}'.format(year, magnet, stream)]['output_type'] = output_type

                options = ['data_type_{}.py'.format(year), 'data.py', 'input_type_{}dst.py'.format('m' if stream=='LEPTONIC' else ''), 'sequence_setup.py']

                if stream == 'DIMUON':
                    config_dict['{}_{}_{}'.format(year, magnet, stream)]['options'] = options+dimuon
                elif stream == 'LEPTONIC':
                    config_dict['{}_{}_{}'.format(year, magnet, stream)]['options'] = options+leptonic
                elif stream == 'BHADRONCOMPLETEEVENT':
                    config_dict['{}_{}_{}'.format(year, magnet, stream)]['options'] = options+bhadron

                current_options = dict()
                current_options.update(bookkeeping_data[year]._asdict())
                current_options['stream'] = stream
                current_options['magnet'] = magnet
                current_options['year'] = year[-2:]
                current_options['dst'] = 'MDST' if stream=='LEPTONIC' else 'DST'
                bookkeeping_path = '/LHCb/Collision{year}/Beam{beam}GeV-VeloClosed-{magnet}/Real Data/{reco}/{strip}/90000000/{stream}.{dst}'.format(**current_options)

                config_dict['{}_{}_{}'.format(year, magnet, stream)]['bookkeeping_path'] = bookkeeping_path


    config_dict_mc  = dict()
    for year in periods:
        for bkk_path in bookkeeping_mc[year]:
            for magnet in ['MagUp', 'MagDown']:
                stream = 'ALLSTREAMS'
                mc_type = bkk_path.mc_type
                mc_id = 'MC_{}_{}_{}_{}'.format(year, magnet, stream, mc_type)

                config_dict_mc[mc_id] = dict()
                config_dict_mc[mc_id]['dq_flag'] = dq_flag
                config_dict_mc[mc_id]['application'] = application
                config_dict_mc[mc_id]['application_version'] = application_version
                config_dict_mc[mc_id]['output_type'] = output_type

                ftag = open('tags/tags_{}.py'.format(mc_id),'w')
                ftag.write('from Configurables import DaVinci \n')
                ftag.write('DaVinci().DDDBtag = "{dddb}" \n'.format(dddb=bkk_path.dddb))
                ftag.write('DaVinci().CondDBtag = "{conddb}-vc-{mag}100" \n'.format(conddb=bkk_path.conddb, mag='mu' if magnet=='MagUp' else 'md'))
                ftag.close()

                options = ['mc.py', 'input_type_{}.py'.format(bkk_path.dst.lower()), 'tags/tags_{}.py'.format(mc_id), 'data_type_{}.py'.format(year), 'sequence_setup.py']

                config_dict_mc[mc_id]['options'] = options+[bkk_path.option_file]

                current_options = dict()
                current_options.update(bkk_path._asdict())
                current_options['stream'] = stream
                current_options['magnet'] = magnet
                current_options['year'] = year
                current_options['pythia'] = bkk_path.pythia
                bookkeeping_path = '/MC/{year}/Beam6500GeV-{year}-{magnet}-Nu1.6-25ns-{pythia}/{sim}/{trigreco}/{strip}NoPrescalingFlagged/{eventtype}/{stream}.{dst}'.format(**current_options)

                if not bkk_path.strip:
                    bookkeeping_path = '/MC/{year}/Beam6500GeV-{year}-{magnet}-Nu1.6-25ns-{pythia}/{sim}/{trigreco}/{dst}'.format(**current_options)
                    config_dict_mc[mc_id]['options'][-1] = '{}_stripping.py'.format(bkk_path.option_file.strip('.py'))
                    config_dict_mc[mc_id]['options'].pop(-2)
                    config_dict_mc[mc_id]['application'] = 'DaVinci'
                    config_dict_mc[mc_id]['application_version'] = bookkeeping_data[year].davinci
                    config_dict_mc[mc_id]['output_type'] = 'CustomStream.ldst'.upper()

                config_dict_mc[mc_id]['bookkeeping_path'] = bookkeeping_path

    config_dict.update(config_dict_mc)

    with open(output_json, 'w') as f:
        json.dump(config_dict, f, indent=4)

